package ru.edu.task3.java;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
public class PromDependency implements DependencyObject {
    @Override
    public String getValue() {
        return "PROM";
    }
}
